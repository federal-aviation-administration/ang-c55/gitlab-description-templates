# ANG-C55 GitLab Description Templates

Templates for use with various GitLab functions (Issues, Merge Requests, etc.)

[[_TOC_]]

# Merge Request Templates


## Standard Merge Request

This template is the "catch-all" for most merge requests. 

### Description

[Standard Merge Request.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/merge_request_templates/Standard%20Merge%20Request.md?viewer=simple)

### Applicable Scenarios

* Branches from and being merged into `develop`


## Stable / Hotfix Release Merge Request

This template is used when the merge will result in a stable release of the project. 

### Title

```
Release <version id>: (Descriptive Release Title, if applicable) 
```

### Description

[Stable or Hotfix Merge Request.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/merge_request_templates/Stable%20or%20Hotfix%20Merge%20Request.md?viewer=simple)

### Applicable Scenarios

* Release branches being merged into `main` \ `master`
* Hotfix branches being merged into `main` \ `master`
* `develop` branch being merged into `main` \ `master`


## Release Candidate Fix/Update Merge Request

This template is used when a release candidate has already been created and requires and update. 

### Description

[Release Candidate Fix or Update Merge Request.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/merge_request_templates/Release%20Candidate%20Fix%20or%20Update%20Merge%20Request.md?viewer=simple)

### Applicable Scenarios

* Branches branched from and being merged into `release-*` branches


# Issue Templates


## Bug

Used to report a bug/defect with a project. 

### Description

[Bug.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/issue_templates/Bug.md?viewer=simple)


## Feature / Enhancement

Used to request a new capability for a project. 

### Description

[Feature or Enhancement.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/issue_templates/Feature%20or%20Enhancement.md?viewer=simple)


# Tag Templates

__Note:__ GitLab does not currently support templating of tag descriptions. The templates included in this section must be manually copied/pasted.


## Stable Release

Used to designate a stable release of a project. 

### Description

[Stable Release.md](https://gitlab.com/federal-aviation-administration/ang-c55/gitlab-description-templates/-/blob/main/.gitlab/tag_templates/Stable%20Release.md?viewer=simple)


# License

## Creative Commons Zero v1.0 Universal

This is a work created by or on behalf of the United States Government. To the extent that this work is not already in
the public domain by virtue of 17 USC § 105, the FAA waives copyright and neighboring rights in the work worldwide
through the CC0 1.0 Universal Public Domain Dedication (which can be found at https://creativecommons.org/publicdomain/zero/1.0/).

See [LICENSE.txt](LICENSE.txt) and [NOTICE.txt](NOTICE.txt) in the root of this project for the full terms of the license.
