## Improvements
* List
* Of
* Improvements

## Bug Fixes
* Related
* Issues

## Potential Breaking Changes
* Changes made with this release that could/will cause consumers of this project (applications, libraries, end users, etc.) to fail when upgrading to this (or a later) release.
* e.g. API change, Major behavioral change, Configuration change, etc.
* Describe the actual breaking changes

## Do the changes in this MR meet the baseline acceptance criteria?
- [ ] Version number updated in documentation (`README.md`, help, etc.)
- [ ] No outstanding issues / merge requests for associated Milestone
- [ ] `LICENSE-3RD-PARTY.txt` updated if dependencies have changed

## Pre-merge Tasks
- [ ] Set target branch to `master`
- [ ] Set appropriate Milestone
- [ ] Set appropriate Labels
  - [x] ~release label is set
/label ~release
- [ ] Set appropriate number of approvers 
- [ ] Uncheck "Delete source branch"
- [ ] Uncheck "Squash Commits"

## Post-merge Tasks
- [ ] Create `<version>` tag
- [ ] Publish/Deploy `<version>` release
- [ ] Merge release/hotfix branch into develop
- [ ] Delete release/hotfix branch
- [ ] Close `<milestone-name>` milestone
