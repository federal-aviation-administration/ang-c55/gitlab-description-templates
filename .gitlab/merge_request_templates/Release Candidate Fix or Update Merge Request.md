## What does this MR do?
* (Description of what this MR does. Should be bulleted list.)

## Are there any changes that require more careful review?
* (Areas of the code (line #(s), methods, etc.), specific concepts, documentation, etc. that the reviewer should look at very closely)

## Screenshots
(If applicable, otherwise remove.)

## What are the relevant issue numbers?
(If applicable, "N/A" otherwise. To auto close issues upon merge, use the phrase "Closes", "Fixes", "Resolve"s, or "Implements" followed by the issue number (or comma separated numbers))
Related to #<Issue Number>
Resolves #<Issue Number>

## Do the changes in this MR meet the baseline acceptance criteria?
- [ ] Tests created for relevant changes
- [ ] Javadoc compiles successfully with no errors or warnings
- [ ] `README` / documentation updated with relevant changes
- [ ] `LICENSE-3RD-PARTY.txt` updated if dependencies have changed
- [ ] All acceptance criteria in relevant issue is met
- [ ] Version number updated in documentation (`README.md`, help, etc.)

## Pre-merge Tasks
- [ ] Set target branch to `release-<version>`
- [ ] Set appropriate Labels
- [ ] Set appropriate number of approvers
- [ ] Set appropriate Milestone
- [ ] Check "Delete source branch"
- [ ] Check "Squash Commits"
- [ ] Set squash commit message to MR title/contents of "What does this MR do?" section

## Post-merge Tasks
- [ ] Release a new Release Candidate (if applicable)
  - [ ] Create `<version>-RC<number>` tag
  - [ ] Publish/Deploy `<version>-RC<number>` release
- [ ] Merge `release-*` branch into `develop`
- [ ] Merge `release-*` branch into other downstream `release-*` branches
