# Release {version}: {Release Title (if applicable} (e.g. "Release 1.4: {release name}" or "Release 1.4.1: {Thing that was fixed})">

## Improvements
* List
* of
* improvements

## Bug Fixes
* Related
* Issues

## Potential Breaking Changes
* Changes that could cause certain programs that use an older version of this project to break upon upgrading to this version or higher

(if this is a bugfix, the previous release notes go here)
